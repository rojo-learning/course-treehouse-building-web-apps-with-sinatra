
require 'sinatra'
require 'uri'

# allows to get the content of the created pages
def page_content(title)
  File.read "pages/#{title}.txt"
rescue Errno::ENOENT
  return nil
end

# allows create new pages
def save_content(title, content)
  File.open "pages/#{title}.txt", "w" do |file|
    file.print content
  end
end

# allows to delete pages
def delete_content(title)
  File.delete "pages/#{title}.txt"
end

# provides the titles of the existent pages
def page_list
  [].tap do |titles_list|
    Dir.foreach 'pages' do |title|
      titles_list << title.chomp('.txt') if title.match /\.txt\z/i
    end
  end
end

# sanitize page content
def scape(string)
  Rack::Utils.escape_html string
end

get "/" do
  # the `erb` command allows to load HTML templates with embedded Ruby code
  erb :welcome
end

# route to get the list of existent pages
get "/index" do
  @pages = page_list
  erb :index
end

get "/new" do
  # this route should appear before /:page_title to have priority
  erb :new
end

# route to try to get a page
get "/:title" do
  @title   = params[:title]
  @content = page_content(@title)
  erb :show
end

# route to retrieve existent page data for editing
get "/:title/edit" do
  @title = params[:title]
  @content = page_content(@title)
  erb :edit
end

# route to create a new page
post "/create" do
  save_content params[:title], params[:content]
  redirect URI.escape("/#{params[:title]}")
end

# route to accept modifications for existent pages
put "/:title" do
  save_content params[:title], params[:content]
  redirect URI.escape("/#{params[:title]}")
end

delete "/:title" do
  delete_content params[:title]
  redirect "/"
end
