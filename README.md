# Treehouse - Building Web Apps with Sinatra #

This repository contains notes and practice examples from the course
**Building Web Apps with Sinatra**, imparted by Jay McGavren at
[Threehouse][JF].

> Sinatra is the second-most popular web framework written in Ruby (after
> Rails). It's much simpler than Rails, and it's a great way to try out web
> development.

## Contents ##

- **Your first Sinatra App**: Sinatra is a simple web framework. We'll discuss
exactly what that means, and how Sinatra can help you build web apps. Then we'll
install Sinatra, and try out a simple app.
- **ERB Templates**: Sinatra has a feature that makes it easy to load text in
from files: ERB templates. ERB stands for "Embedded Ruby". It allows you to take
a text file, such as an HTML template, that always shows the same static text,
and embed Ruby code into it, that displays changing dynamic data.
- **Adding New Data**: Our users can view pages that we've created for them. But
 a major feature of a wiki is the ability for users to create their own pages,
and they can't do that yet. So let's fix that. Our next task is to set up a form
so that users can create their own pages.
- **Updating Data**: In this stage, we'll need to set up another pair of new
Sinatra routes. The first route will accept GET requests and return an HTML form
for a wiki page, just like before. The difference is that this HTML form will be
pre-populated with an existing page's content. The second route will accept the
completed form data, also just like before. But since we're updating existing
data on the server, we need to send a PUT request, not a POST request.
- **Finishing Touches**: We can add new wiki pages, show pages we've added, and
then update them. The only thing missing now is the ability to delete pages.
Let's add that now. We'll also show you how to serve a static CSS file to make
your site look more appealing. Finally, we'll link to that CSS file from every
page on our site using a Sinatra layout.

## Projects ##

- **Wiki**
- **Guestbook**

---
This repository contains code examples from Treehouse. These are included under
fair use for showcasing purposes only. Those examples may have been modified to
fit my particular coding style.

[JF]: http://teamtreehouse.com
