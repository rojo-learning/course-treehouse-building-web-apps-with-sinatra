
require 'sinatra'

# appends the signature to the file in an new line
def save_signature(signature)
  File.open 'signatures.txt', 'a' do |file|
    file.puts signature
  end
end

# loads signature at the given line index
def load_signature(index)
  File.readlines('signatures.txt')[index.to_i].chomp
end

# returns all the signatures
def load_signatures
  File.readlines('signatures.txt')
end

# updates the signature at the given index
def update_signature(index, signature)
  lines = File.readlines 'signatures.txt'
  lines[index.to_i] = signature

  File.open 'signatures.txt', 'w' do |file|
    file.puts lines
  end
end

# removes a signature at the given index
def delete_signature(index)
  lines = File.readlines 'signatures.txt'
  lines.delete_at index.to_i

  File.open 'signatures.txt', 'w' do |file|
    file.puts lines
  end
end

# sanitize retrieved user provided content
def scape(string)
  Rack::Utils.escape_html string
end

get '/' do
  erb :welcome
end

get '/index' do
  @signatures = load_signatures
  erb :index
end

get '/new' do
  erb :new
end

get '/:index' do
  @index = params[:index]
  @signature = load_signature(@index)
  erb :show
end

get '/:index/edit' do
  @index = params[:index]
  @signature = load_signature(@index)
  erb :edit
end

post '/create' do
  save_signature params[:signature]
  redirect '/index'
end

put '/:index' do
  update_signature params[:index], params[:signature]
  redirect "/#{params[:index]}"
end

delete '/:index' do
  delete_signature params[:index]
  redirect '/'
end
